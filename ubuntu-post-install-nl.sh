#!/bin/bash
sudo apt update -y && sudo apt upgrade -y
sudo apt install -y ubuntu-restricted-extras
sudo apt install -y unrar zip unzip p7zip-full p7zip-rar rar
sudo apt install -y vlc
sudo apt install -y shotwell
sudo apt install -y libreoffice
sudo apt install -y thunderbird
sudo apt install -y calibre
sudo apt install -y clementine
sudo apt autoremove
sudo apt install -y hunspell-nl
sudo apt install -y libreoffice-l10n-nl libreoffice-help-nl
sudo apt install -y firefox-locale-nl
sudo apt install -y thunderbird-locale-nl
