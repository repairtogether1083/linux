#!/bin/bash
su -c 'apt install -y sudo'
sudo apt-add-repository non-free
sudo apt-add-repository contrib
echo "http://deb.debian.org/debian buster-backports main" | sudo tee /etc/apt/sources.list.d/deb-backports.list
sudo apt update
sudo apt upgrade
sudo apt install -y libavcodec-extra
sudo apt install -y pepperflashplugin-nonfree
sudo update-pepperflashplugin-nonfree --install
sudo apt install -y browser-plugin-freshplayer-pepperflash
sudo apt install -y firmware-linux firmware-linux-nonfree
sudo apt install -y unrar zip unzip p7zip-full p7zip-rar rar
sudo apt install -y vlc
sudo apt install -y shotwell
sudo apt install -y libreoffice
sudo apt install -y thunderbird
sudo apt install -y calibre
sudo apt install -y clementine
sudo apt autoremove
sudo apt install -y hunspell-fr
sudo apt install -y hunspell-nl
sudo apt install -y libreoffice-l10n-fr libreoffice-help-fr
sudo apt install -y libreoffice-l10n-nl libreoffice-help-nl
wget -P /tmp https://grammalecte.net/grammalecte/oxt/Grammalecte-fr-v1.6.0.oxt
sudo apt install -y firefox-locale-fr
sudo apt install -y firefox-locale-nl
sudo apt install -y thunderbird-locale-fr
sudo apt install -y thunderbird-locale-nl
sudo apt install -y gnome-shell-extensions gnome-menus gir1.2-gmenu-3.0
sudo apt install -y gnome-tweak-tool
