#!/bin/bash
sudo apt update && sudo apt upgrade
sudo apt install -y mint-meta-codecs xubuntu-restricted-extras
sudo apt remove -y timeshift
sudo apt install -y pepperflash-player
sudo apt install -y browser-plugin-freshplayer-pepperflash
sudo apt install -y unrar zip unzip p7zip-full p7zip-rar rar
sudo apt install -y vlc
sudo apt install -y mpv
sudo add-apt-repository ppa:libreoffice/ppa && sudo apt update -y
sudo apt install -y libreoffice
sudo apt install -y thunderbird
sudo apt install -y calibre
sudo apt install -y shotwell
sudo apt autoremove
sudo apt install -y hunspell-fr
sudo apt install -y hunspell-nl
sudo apt install -y libreoffice-l10n-fr libreoffice-help-fr
sudo apt install -y libreoffice-l10n-nl libreoffice-help-nl
wget -P /tmp https://grammalecte.net/grammalecte/oxt/Grammalecte-fr-v1.6.0.oxt
sudo apt install -y firefox-locale-fr firefox-esr-l10n-fr
sudo apt install -y firefox-locale-nl
sudo apt install -y thunderbird-locale-fr thunderbird-l10n-fr
sudo apt install -y thunderbird-locale-nl
wget -P /tmp "https://dllb2.pling.com/api/files/download/j/eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjE1MzA3NzQ2MDAiLCJ1IjpudWxsLCJsdCI6ImRvd25sb2FkIiwicyI6IjkwZGI5MTM2MmU0NjUzZDI1YzgzMzUwYTE4M2UzOTMyYjY2MmY5NTQ5MmQxMjk3ZTdkMjk4NTQ2NDI5NWFkNDM3NmIxMGE1Yjg4YzU5NTBhODJkN2Y2ZWNkOTZkNjJmNmIzMGUxMzAwOWRmYzgxZjNmZWQ0YTQzOTYzZTc2ZDE0IiwidCI6MTU4Mjk5NDE3MSwic3RmcCI6IjgyMGRlNTk5YzQzZjZjMGMwYWE0MjU4NjNiN2JlYzdmIiwic3RpcCI6Ijg3LjY1LjEwOC4xNDkifQ.7MEyVVAgiqOeZOjkYabeLCs3S06IGP8EyTnFSscOQpk/ocs-url_3.1.0-0ubuntu1_amd64.deb"
