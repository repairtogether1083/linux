#!/bin/bash
sudo apt update -y && sudo apt upgrade -y
sudo apt install -y ubuntu-restricted-extras
sudo apt install -y unrar zip unzip p7zip-full p7zip-rar rar
sudo apt install -y vlc
sudo apt install -y shotwell
sudo apt install -y libreoffice
sudo apt install -y thunderbird
sudo apt install -y calibre
sudo apt install -y clementine
sudo apt autoremove
sudo apt install -y hunspell-fr
sudo apt install -y libreoffice-l10n-fr libreoffice-help-fr
wget -P /tmp https://grammalecte.net/grammalecte/oxt/Grammalecte-fr-v1.6.0.oxt
sudo apt install -y firefox-locale-fr firefox-esr-l10n-fr
sudo apt install -y thunderbird-locale-fr thunderbird-l10n-fr
