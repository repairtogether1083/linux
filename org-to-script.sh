#!/bin/bash
set -u

function gen_script {
  local -r post_install_script=$1
  shift
  local -a srcs=( "$@" )

  echo '#!/bin/bash' > ${post_install_script}
  for src in "${srcs[@]}"; do
    sed -n 's/^ *- $ *\(.*\)$/\1/p' ${src} >> ${post_install_script}
    case ${lang} in
      all) sed -n 's/^ *- \(fr\|nl\): $ *\(.*\)$/\2/p' ${src} >> ${post_install_script} ;;
      fr) sed -n 's/^ *- fr: $ *\(.*\)$/\1/p' ${src} >> ${post_install_script} ;;
      nl) sed -n 's/^ *- nl: $ *\(.*\)$/\1/p' ${src} >> ${post_install_script} ;;
      *) echo "Error: use 'fr' or 'nl' as second argument" >&2
         exit 1
         ;;
    esac
  done
  chmod 755 ${post_install_script}
}

function gen_manual_steps {
  local -r post_install_script=$1
  shift
  local -a srcs=( "$@" )

  for src in "${srcs[@]}"; do
    sed -n 's/^ *- man: $ *\(.*\)$/echo "\1"/p' ${src} >> ${post_install_script}
  done
}

function main {
  local -r de=$1

  local -a srcs
  case ${de} in
    mxlinux-gnome)
      srcs=( 02-mxlinux-setup.org 03-mxlinux-gnome-setup.org 04-applications.org
             05-mxlinux-gnome.org )
      ;;
    ubuntu)
      srcs=( 01-ubuntu-minimal.org 02-ubuntu-setup.org 04-applications.org)
      ;;
    kubuntu)
      srcs=( 01-ubuntu-minimal.org 02-kubuntu-setup.org 04-applications.org
             05-kubuntu-setup.org)
      ;;
    mint-xfce)
      srcs=( 01-mint-xfce.org 04-applications.org 05-mint-xfce-setup.org)
      ;;
    *) echo "Error: use 'debian-gnome' or 'kubuntu-minimal' as first argument" >&2
       exit 1
  esac
  srcs+=(  )

  local post_install_script
  for lang in all fr nl; do
    post_install_script=${de}-post-install-${lang}.sh
    gen_script ${post_install_script} "${srcs[@]}"
  done

  local post_install_manual=${de}-post-install-manual.sh
  gen_manual_steps ${post_install_manual} "${srcs[@]}"

  local post_install_text=${de}-post-install.org
  echo "" > ${post_install_text}
  for src in "${srcs[@]}"; do
    grep -v '\( \$\|^$\)' "${src}" >> ${post_install_text}
  done
}

main "$@"
